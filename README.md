# Superlative #



### What is Superlative? ###

**Superlative** is an automated **NLP** (**N**atural **L**anguage **P**rocessing) writing feedback tool that 
performs text segmentation and classification of argumentative & rhetorical elements to improve the quality of essay writing. 

**Superlative** is a pedagogic tool that generates feedback and suggestions for student essays to elevate writing proficiency.